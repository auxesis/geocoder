import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geocoder/services/base.dart';

import 'widgets.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class AppState extends InheritedWidget {
  const AppState({
    Key? key,
    required this.mode,
    required Widget child,
  }) : super(
          key: key,
          child: child,
        );

  final Geocoding mode;

  static AppState of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<AppState>()!;
  }

  @override
  bool updateShouldNotify(AppState oldWidget) => mode != oldWidget.mode;
}

class GeocodeView extends StatefulWidget {
  const GeocodeView({Key? key}) : super(key: key);

  @override
  _GeocodeViewState createState() => _GeocodeViewState();
}

class _GeocodeViewState extends State<GeocodeView> {
  _GeocodeViewState();

  final TextEditingController _controller = TextEditingController();

  List<Address> results = [];

  bool isLoading = false;

  Future search() async {
    setState(() {
      isLoading = true;
    });

    try {
      var geocoding = AppState.of(context).mode;
      var results = await geocoding.findAddressesFromQuery(_controller.text);
      setState(() {
        this.results = results;
      });
    } catch (e) {
      if (kDebugMode) {
        print("Error occured: $e");
      }
    } finally {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Card(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: TextField(
                    controller: _controller,
                    decoration: const InputDecoration(
                      hintText: "Enter an address",
                    ),
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.search),
                  onPressed: () => search(),
                )
              ],
            ),
          ),
        ),
        Expanded(
          child: AddressListView(
            isLoading,
            results,
          ),
        ),
      ],
    );
  }
}

class ReverseGeocodeView extends StatefulWidget {
  const ReverseGeocodeView({Key? key}) : super(key: key);

  @override
  _ReverseGeocodeViewState createState() => _ReverseGeocodeViewState();
}

class _ReverseGeocodeViewState extends State<ReverseGeocodeView> {
  final TextEditingController _controllerLongitude = TextEditingController();
  final TextEditingController _controllerLatitude = TextEditingController();

  _ReverseGeocodeViewState();

  List<Address> results = [];

  bool isLoading = false;

  Future search() async {
    setState(() {
      isLoading = true;
    });

    try {
      var geocoding = AppState.of(context).mode;
      var longitude = double.parse(_controllerLongitude.text);
      var latitude = double.parse(_controllerLatitude.text);
      var results = await geocoding
          .findAddressesFromCoordinates(Coordinates(latitude, longitude));
      setState(() {
        this.results = results;
      });
    } catch (e) {
      if (kDebugMode) {
        print("Error occured: $e");
      }
    } finally {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Card(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  children: <Widget>[
                    TextField(
                      controller: _controllerLatitude,
                      decoration: const InputDecoration(
                        hintText: "Latitude",
                      ),
                    ),
                    TextField(
                      controller: _controllerLongitude,
                      decoration: const InputDecoration(
                        hintText: "Longitude",
                      ),
                    ),
                  ],
                ),
              ),
              IconButton(
                icon: const Icon(Icons.search),
                onPressed: () => search(),
              )
            ],
          ),
        ),
      ),
      Expanded(
        child: AddressListView(
          isLoading,
          results,
        ),
      ),
    ]);
  }
}

class _MyAppState extends State<MyApp> {
  Geocoding geocoding = Geocoder.local;

  final Map<String, Geocoding> modes = {
    "Local": Geocoder.local,
    "Google (distant)": Geocoder.google("<API-KEY>"),
  };

  void _changeMode(Geocoding mode) {
    setState(() {
      geocoding = mode;
    });
  }

  @override
  Widget build(BuildContext context) {
    return AppState(
      mode: geocoding,
      child: MaterialApp(
        home: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: AppBar(
              title: const Text('Geocoder'),
              actions: <Widget>[
                PopupMenuButton<Geocoding>(
                  // overflow menu
                  onSelected: _changeMode,
                  itemBuilder: (BuildContext context) {
                    return modes.keys.map((String mode) {
                      return CheckedPopupMenuItem<Geocoding>(
                        checked: modes[mode] == geocoding,
                        value: modes[mode],
                        child: Text(mode),
                      );
                    }).toList();
                  },
                ),
              ],
              bottom: const TabBar(
                tabs: [
                  Tab(
                    text: "Query",
                    icon: Icon(Icons.search),
                  ),
                  Tab(
                    text: "Coordinates",
                    icon: Icon(Icons.pin_drop),
                  ),
                ],
              ),
            ),
            body: const TabBarView(
              children: <Widget>[
                GeocodeView(),
                ReverseGeocodeView(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
