import 'package:flutter/material.dart';
import 'package:geocoder/model.dart';

class AddressTile extends StatelessWidget {
  final Address address;

  const AddressTile(this.address, {Key? key}) : super(key: key);

  final titleStyle = const TextStyle(
    fontSize: 15.0,
    fontWeight: FontWeight.bold,
  );

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ErrorLabel(
              'feature name',
              address.featureName,
              fontSize: 15.0,
              isBold: true,
            ),
            ErrorLabel('address lines', address.addressLine),
            ErrorLabel('country name', address.countryName),
            ErrorLabel('locality', address.locality),
            ErrorLabel('sub-locality', address.subLocality),
            ErrorLabel('admin-area', address.adminArea),
            ErrorLabel('sub-admin-area', address.subAdminArea),
            ErrorLabel('thoroughfare', address.thoroughfare),
            ErrorLabel('sub-thoroughfare', address.subThoroughfare),
            ErrorLabel('postal code', address.postalCode),
            address.coordinates != null
                ? ErrorLabel("", address.coordinates.toString())
                : ErrorLabel("coordinates", null),
          ]),
    );
  }
}

class AddressListView extends StatelessWidget {
  final List<Address> addresses;

  final bool isLoading;

  const AddressListView(this.isLoading, this.addresses, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return const Center(child: CircularProgressIndicator());
    }

    return ListView.builder(
      itemCount: addresses.length,
      itemBuilder: (c, i) => AddressTile(addresses[i]),
    );
  }
}

class ErrorLabel extends StatelessWidget {
  final String name;
  final String text;

  final TextStyle descriptionStyle;

  ErrorLabel(this.name, String? text,
      {Key? key, double fontSize = 9.0, bool isBold = false})
      : text = text ?? 'Unknown $name',
        descriptionStyle = TextStyle(
            fontSize: fontSize,
            fontWeight: isBold ? FontWeight.bold : FontWeight.normal,
            color: text == null ? Colors.red : Colors.black),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text, style: descriptionStyle);
  }
}
