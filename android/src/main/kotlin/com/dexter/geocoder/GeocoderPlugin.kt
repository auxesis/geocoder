package com.dexter.geocoder

import android.content.Context
import android.location.Address
import android.location.Geocoder
import androidx.annotation.NonNull
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import java.io.IOException
import java.util.*

/**
 * NotAvailableException
 */
internal class NotAvailableException : Exception()

/**
 * GeocoderPlugin
 */
class GeocoderPlugin : FlutterPlugin, MethodCallHandler {
  private var channel: MethodChannel? = null
  private var context: Context? = null
  private var geocoder: Geocoder? = null

  @Override
  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "geocoder")
    channel?.setMethodCallHandler(this)
    context = flutterPluginBinding.applicationContext
    geocoder = Geocoder(context)
  }

  @Override
  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    when {
      call.method.equals("findAddressesFromQuery") -> {
        val address = call.argument("address") as String?
        findAddressesFromQuery(address, result)
      }
      call.method.equals("findAddressesFromCoordinates") -> {
        val latitude: Double = (call.argument("latitude") as Number?)?.toDouble() ?: 0.0
        val longitude: Double = (call.argument("longitude") as Number?)?.toDouble() ?: 0.0
        findAddressesFromCoordinates(latitude, longitude, result)
      }
      else -> {
        result.notImplemented()
      }
    }
  }

  @Override
  override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
    channel?.setMethodCallHandler(null)
  }

  @Throws(NotAvailableException::class)
  private fun assertPresent() {
    if (!Geocoder.isPresent()) {
      throw NotAvailableException()
    }
  }

  private fun findAddressesFromQuery(address: String?, result: Result) {
    val plugin = this
    Thread {
      val addresses : ArrayList<Address> = ArrayList()
      try {
        plugin.assertPresent()
        geocoder?.let { addresses.addAll(it.getFromLocationName(address, 20)) }
      } catch (ex: IOException) {
        addresses.clear()
      } catch (ex: NotAvailableException) {
        addresses.clear()
      }
      if (addresses.isEmpty()) result.error("not_available", "Empty", null) else result.success(createAddressMapList(addresses))
    }.start()
  }

  private fun findAddressesFromCoordinates(latitude: Double, longitude: Double, result: Result) {
    val plugin = this
    Thread {
      val addresses : ArrayList<Address> = ArrayList()
      try {
        plugin.assertPresent()
        geocoder?.let { addresses.addAll(it.getFromLocation(latitude, longitude, 20)) }
      } catch (ex: IOException) {
        addresses.clear()
      } catch (ex: NotAvailableException) {
        addresses.clear()
      }
      if (addresses.isEmpty()) result.error("not_available", "Empty", null) else result.success(createAddressMapList(addresses))
    }.start()
  }

  private fun createCoordinatesMap(address: Address?): Map<String, Any>? {
    if (address == null) return null
    val result: MutableMap<String, Any> = HashMap()
    result["latitude"] = address.latitude
    result["longitude"] = address.longitude
    return result
  }

  private fun createAddressMap(address: Address?): Map<String, Any?>? {
    if (address == null) return null

    // Creating formatted address
    val sb = StringBuilder()
    for (i in 0..address.maxAddressLineIndex) {
      if (i > 0) {
        sb.append(", ")
      }
      sb.append(address.getAddressLine(i))
    }
    val result: MutableMap<String, Any?> = HashMap()
    result["coordinates"] = createCoordinatesMap(address)
    result["featureName"] = address.featureName
    result["countryName"] = address.countryName
    result["countryCode"] = address.countryCode
    result["locality"] = address.locality
    result["subLocality"] = address.subLocality
    result["thoroughfare"] = address.thoroughfare
    result["subThoroughfare"] = address.subThoroughfare
    result["adminArea"] = address.adminArea
    result["subAdminArea"] = address.subAdminArea
    result["addressLine"] = sb.toString()
    result["postalCode"] = address.postalCode
    return result
  }

  private fun createAddressMapList(addresses: List<Address>?): List<Map<String, Any?>?> {
    if (addresses == null) return ArrayList()
    val result: MutableList<Map<String, Any?>?> = ArrayList(addresses.size)
    for (address in addresses) {
      result.add(createAddressMap(address))
    }
    return result
  }
}